#### Code ####
print("<-~ Simplay Dev Tools ~->")

tools = ["world editor"]

print("")
for i in tools:
    print(i)
print("")

tool = input("What tool would you like to use?: ").lower()

print("")

if tool == tools[0]:
    from world_editor import *

else:
    print("Not a valid tool!")