import json
import os

prompt_symbol = "  #> "

print("Please input the path to your world file's parent directory.")
world_path = input(prompt_symbol)

print("\nPlease input the world name (without .swd on the end).")
world = f"{input(prompt_symbol)}.swd"

try:
    os.chdir(world_path)

except:
    print("\nInvalid path!")
    exit()

try:
    with open(world, "r") as file:
        world_data = json.load(file)

except:
    print("Failed to load that world!")
    exit()

focus = "null"

print("\nWhat would you like to focus on?")
print('''
seed
''')
focus = input(prompt_symbol).lower()

if focus == "seed":
    print("")
    print(world_data.get("seed"))
    print("\nNew Seed (leave blank to cancel)")
    seed = input(prompt_symbol)
    print("")
    if seed != "":
        world_data["seed"] = int(seed)
    else:
        print("Aborting...")

else:
    print("Invalid option!")
    exit()

print("Saving world... please wait.")
with open(world, "w") as file:
    json.dump(world_data, file)
print("Done!")